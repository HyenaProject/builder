Builder
=======

This is starting as a copy of the gNewSense builder script, revision 517.

We are keeping the name, but will do a lot of changes, fixes and hacks on the scripts.
We are doing this because they _just simply work_. And it's pretty easy to use.

These scripts automatically rebrand, rebuild and build what's needed for a complete operating system.
This includes updates and any software we decide that is needed.

Of course, our mods, hacks and fixes will be biased towards the Hyena OS project. But that should be ok for others to use, if not and you want the r517 original then ask for it and we'll give it to you.


Documentation
=============

Most of that will be kept in the wiki here.


License
=======

The original scripts were released under GPLv2, we will upgrade our changes to GPLv3.

See the LICENSE for details.



Versioning
==========

We will use a [Semantic Versioning](http://semver.org/)

Given a version number MAJOR.MINOR.PATCH, increment the:

    MAJOR version when you make incompatible API changes,
    MINOR version when you add functionality in a backwards-compatible manner, and
    PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

We will tag as 0.0.1, 0.0.2, 0.0.3 ... until we think a 1.0.0 is ready for testing in which we will branch that as 1.x branch and the tag that until it reaches the 1.1.x branch.

The master branch will be our development branch.
